package com.company;
import java.util.Scanner;
/**
 Дано расстояние в сантиметрах. Найти число полных метров в нем.
 */
public class zadacha_2_1 {
    public static void main(String args[]){
        Scanner in = new Scanner(System.in);
        System.out.print("Vvedute chuslo a: ");
        int a = in.nextInt();
        int d = a / 100;
        int c = a % 100;
        System.out.println("ostatok " + c + " cm");
        System.out.println("vo vvedennom chsle " + d + " m");
    }
}
